# DOODL Static Website Boilerplate

This repo is intended to be used for quickly creating a static website/landing page using HTML/SCSS and the [@doodl/slate](https://www.npmjs.com/package/@doodl/slate) framework.

## Prerequisites
- Node + NVM (Node Version Manager) installed locally
- Yarn (preferred) or NPM

## Getting Started
1. Clone this repository onto your local machine OR download into an empty folder without any Git history by running `git archive --remote git@bitbucket.org:doodlltd/doodl-static-website-boilerplate.git --format tar master | tar xf -` in your terminal.
2. To ensure you're running the project with a compatible version of Node, run the command `nvm use`.  This will either result in your environment switching to the correct version of Node, or you being prompted to install it.
3. Run either `yarn install` or `npm install` to install the dependencies.

## Begin Developing
To start the development server, run `yarn run dev` or `npm run dev`.

You can now begin making changes to HTML templates inside the **src/template/views*** folder.

> JS and CSS from the Webpack entry point will automatically be injected

### Styling the webpages
You should begin styling from the **src/scss/styles.scss** root file.  Any SCSS variables should be configured inside **src/scss/partials/_config.scss** as these will be exposed to all other included SCSS partials.  This includes overriding all [@doodl/slate](https://www.npmjs.com/package/@doodl/slate) variables.

Use SCSS partials to keep your styles clean and organised.

> Slate styling modules can be enabled/disabled via the **src/scss/partials/_slate-imports.scss** partial.  **Use Slate classes wherever possible to keep your custom CSS as simple as possible**

### Writing Javascript
You can add javascript to your pages via the **src/js/index.js** entry points.  This will be safely transpiled by our standard Babel configuration.

### Code Linting
Automatic code linting will occur in development mode.  For SCSS files, this may also include simple fixes being automatically applied, such as applying best-practice to the ordering of styling rules.

## Building for Deployment
To create a static build for deployment, run `yarn run build` or `npm run build`.