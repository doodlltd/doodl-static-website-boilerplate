import WebFont from "webfontloader";

// Async loading of a Google Webfont
WebFont.load({
    google: {
        families: ['https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,700;1,400;1,700&display=swap'],
    }
});