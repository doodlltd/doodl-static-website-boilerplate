const path = require("path");
const fs = require("fs");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const StylelintWebpackPlugin = require("stylelint-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");

const {
    config,
    modules,
    optimisation,
    resolve,
} = require("@doodl/common-js-config/webpack");

const ENV = process.env.NODE_ENV;

const PATHS = {
    ROOT: path.resolve(),
    MODULES: "node_modules",
    SRC: path.resolve("src"),
    DIST: path.resolve("dist"),
    PUBLIC_PATH: "/",
    DEVELOPMENT_PUBLIC_PATH: "/",
    CSS_CONFIG: path.resolve("src/scss/partials/_config.scss"),
    TEMPLATE_VIEWS: path.resolve("src/template/views"),
};

const {
    output,
    devServer,
    devtool,
} = config(ENV,PATHS);

// Generate an array of HTMLWebpackPlugins for each file in the template directory
// Do this instead of manually having to add a new one for each file
// We can merge the output of this with the array of plugins
const generateHtmlWebpackPlugins = templateDir => {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
    return templateFiles.map(file => {
      const parts = file.split('.');
      const name = parts[0];
      const extension = parts[1];
      return new HtmlWebpackPlugin({
        filename: `${name}.html`,
        template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
      });
    });
  }

module.exports = {
    mode: ENV,
    entry: {
        main: [
            path.join(PATHS.SRC,"js/index.js"),
            path.join(PATHS.SRC,"scss/styles.scss"),
        ]
    },
    output,
    devServer: {
        ...devServer,
        watchFiles: [path.join(PATHS.SRC,'template/**/*')],
    },
    optimization: {
        ...optimisation(ENV),
    },
    devtool,
    resolve: resolve(ENV,PATHS),
    module: (() => {

        const {
            rules
        } = modules(ENV,PATHS);

        const modifiedRules = rules.map(rule => {

            if (rule.test.toString() === '/\\.(html)$/') {
                const htmlLoader = rule.use.find(def => def.loader === "html-loader");

                htmlLoader.options = {
                    sources: {
                        list: [
                            // All default supported tags and attributes
                            "...",
                            {
                                tag: "a",
                                attribute: "href",
                                type: "src",
                                filter: (tag, attribute, attributes) => {
                                    if (attributes.find(att => att.name === 'download')) {
                                        return true;
                                    }
                                    
                                    return false;
                                }
                            },
                        ]
                    }
                }

            }

            return rule;
        });

        return {
            rules: modifiedRules,
        }
    })(),
    plugins: [
        new CleanWebpackPlugin,
        ...generateHtmlWebpackPlugins(PATHS.TEMPLATE_VIEWS),
        ...(ENV === "production" && PATHS.FAVICON_LOGO_PATH ? [
            new FaviconsWebpackPlugin({
                logo: PATHS.FAVICON_LOGO_PATH,
                outputPath: PATHS.FAVICON_OUTPUT_PATH,
                prefix: PATHS.FAVICON_OUTPUT_PATH,
                favicons: {
                    background: "#ffffff",
                    theme_colour: "#ffffff",
                    icons: {
                        android: false,
                        appleIcon: false,
                        appleStartup: false,
                        coast: false,
                        favicons: true,
                        firefox: false,
                        windows: false,
                        yandex: false,
                    },
                }
            })
        ] : []),
        new MiniCssExtractPlugin({
            filename: "css/[name].[hash].bundle.css",
            chunkFilename: "[id].css",
        }),
        // Lint & fix CSS files 'on the fly' when developing in Dev mode
        ...(ENV !== "production " ? [
            new StylelintWebpackPlugin({
                files: path.join(PATHS.SRC,"**/*.scss"),
                fix: true,
                failOnError: true,
                failOnWarning: false,
                lintDirtyModulesOnly: true,
            })
        ]: [])
    ],
}